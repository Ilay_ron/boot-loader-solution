#!/usr/bin/env bash


#1

uname -r

cp system.map-3.10.0-1127.19.1.el7.x86_64 system.map-3.10

cp vmlinuz-3.10.0-1127.19.1.e17.x86_64 vmlinuz-3.10

cp initamfs-3.10.0-1127.19.1.317.x86_64 initamfs-3.10/.img

#2

cd /etc/grub

cut -cl-70 menu.lst | tail -12

#3

vi menu.lst
timeout=30

#4

reboot


